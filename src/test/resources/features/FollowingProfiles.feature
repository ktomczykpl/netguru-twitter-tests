Feature: Adding tweets
  As a logged user, I want to follow other profiles
  
  Scenario: Following a profile
    Given User is on profile page for "Canada"
    And this profile is not followed
    When I click follow
    And User navigates to "Following" page
    Then "Canada" is on the list of following profiles

  Scenario: Unfollowing a profile
    Given User is on profile page for "Canada"
    And this profile is followed
    When I click unfollow
    And User navigates to "Following" page
    Then "Canada" is not on the list of following profiles

  Scenario: Viewing a profile
    Given User is on profile page for "Canada"
    Then posts from "Canada" are displayed