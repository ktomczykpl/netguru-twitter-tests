Feature: Adding tweets
  As a logged user I want to add tweets, delete, like, comment and retweet
  
  Scenario: Add a simple tweet
    Given User is on "Home" page
    When Tweet text "hello there" is entered
    And Tweet button is clicked
    Then new tweet is added with text "hello there"

  Scenario: Add a tweet with URL
    Given User is on "Home" page
    When Tweet text "Check this out www.bbc.com" is entered
    And Tweet button is clicked
    Then new tweet is added with text "Check this out"
    And the new tweet has a preview with title "BBC - Homepage" and domain "bbc.com"

  Scenario: Tweets longer than 280 characters are not allowed
    Given User is on "Home" page
    When Tweet with length of 281 characters is entered
    Then Tweet button is disabled

  Scenario: Delete a tweet
    Given User is on "Home" page
    And a tweet owned by User is displayed
    When User clicks Delete in tweet menu
    Then the tweet is deleted

  Scenario: Like a tweet
   Given User is on "Home" page
    And a tweet owned by User is displayed
    When User clicks Like
    Then the tweet is liked
    When User clicks Unlike
    Then the tweet is not liked

  Scenario: Comment a tweet
    Given User is on "Home" page
    And a tweet owned by User is displayed
    When User clicks add comment button
    And User enters tweet comment "hello"
    Then new tweet is added with text "hello"

  Scenario: Display tweet statistics
    Given User is on "Home" page
    And a tweet owned by User is displayed
    When User clicks statistics button
    Then statistics for tweet are displayed