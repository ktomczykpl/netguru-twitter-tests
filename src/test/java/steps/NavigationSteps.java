package steps;

import base.BasePage;
import base.BaseTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import pages.ProfilePage;

public class NavigationSteps extends BaseTest {

    @Given("^User is on \"([^\"]*)\" page$")
    @And("^User navigates to \"([^\"]*)\" page$")
    public void userIsOnPage(String pageName) {
        BasePage page = pages.getPageByName(pageName);
        page.get();
        context.currentPage = page;
    }

    @Given("^User is on profile page for \"(.*)\"$")
    public void userIsOnProfilePageFor(String profile) {
        ProfilePage page = new ProfilePage(context.driverManager.getDriver(), profile);
        page.get();
        context.currentPage = page;
    }
}
