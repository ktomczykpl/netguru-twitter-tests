package steps;

import base.BaseTest;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import managers.Context;
import pages.LoginPage;
import utilities.FailureHandler;
import utilities.PropertiesLoader;

public class Hooks extends BaseTest {

    private FailureHandler failureHandler = new FailureHandler();

    @Before
    public void setupTestCase() {
        failureHandler.setUpScreenRecorder();
        log.debug(String.format("%s -> Starting tests...", getClass().getName()));
        BaseTest.context = new Context();
        BaseTest.pages = context.pages;
        BaseTest.zapManager = context.zapManager;
        BaseTest.jsExecutor = context.jsExecutor;
        failureHandler.startVideoRecord();
        context.driverManager.initDriver();

        PropertiesLoader propertiesLoader = new PropertiesLoader();
        LoginPage loginPage = pages.getLoginPage();
        loginPage.get();
        loginPage.login(propertiesLoader.getUsername(), propertiesLoader.getPassword());
    }

    @After
    public void ceaseTestCase(Scenario scenario) {
        if (scenario.isFailed()) {
            failureHandler.takePageSource(scenario);
            failureHandler.takeScreenshot(scenario);
        }
        failureHandler.stopVideoRecord(scenario);
        context.driverManager.quit();
        log.debug(String.format("%s -> Ending tests...", getClass().getName()));
    }

}
