package steps;

import base.BaseTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import managers.Context;
import ngelements.NGTweet;
import org.junit.Assert;
import org.openqa.selenium.support.ui.ExpectedConditions;
import pages.HomePage;

import java.util.Date;

public class HomePageSteps extends BaseTest {
    private HomePage homePage = pages.getHomePage();
    private long tweetTime;
    private NGTweet tweetToBeDeleted;

    @When("^Tweet text \"(.*)\" is entered$")
    public void enterTweetText(String text) {
       addTweet(text);
    }

    @When("^Tweet with length of (\\d+) characters is entered$")
    public void tweetWithLengthOfCharactersIsEntered(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append("x");
        }
        homePage.enterTweetText(sb.toString());
    }

    @And("^Tweet button is clicked$")
    public void sendButtonIsClicked() {
        homePage.clickSend();
    }

    @Then("^new tweet is added with text \"(.*)\"$")
    public void verifyTweetText(String text) {
        String expected = String.format("[%d] %s", tweetTime, text);
        Assert.assertEquals(expected, homePage.getTweets().get(0).getContent());
    }

    @And("^a tweet owned by User is displayed$")
    public void addTestTweet() {
        addTweet("hello there");
        homePage.clickSend();
    }

    @When("^User clicks Delete in tweet menu$")
    public void deleteLatestTweet() {
        tweetToBeDeleted = getLatestTweet();
        tweetToBeDeleted.deleteTweet();
    }

    @Then("^the tweet is deleted$")
    public void verifyTweetIsDeleted() {
        Context.driverManager.wait.until(ExpectedConditions.stalenessOf(tweetToBeDeleted.getWrappedElement()));
    }

    @And("^User enters tweet comment \"(.*)\"$")
    public void userEntersTweetComment(String comment) {
        tweetTime = (new Date()).getTime();
        String markedText = String.format("[%d] %s", tweetTime, comment);
        homePage.enterTweetComment(markedText);
    }

    @Then("^Tweet button is disabled$")
    public void tweetButtonIsDisabled() {
        Assert.assertNotNull(homePage.getSendButton().getAttribute("disabled"));
    }

    private void addTweet(String text) {
        // Twitter does not allow adding the same tweet again, so mark it with time
        // better solution: use Twitter API to clean up all tweets before tests
        tweetTime = (new Date()).getTime();
        String markedText = String.format("[%d] %s", tweetTime, text);
        homePage.enterTweetText(markedText);
    }
}
