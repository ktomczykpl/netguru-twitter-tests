package steps;

import base.BaseTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ngelements.NGTweet;
import org.junit.Assert;
import pages.HomePage;

import java.util.List;

public class FeedPageSteps extends BaseTest {
    private HomePage homePage = pages.getHomePage();

    @And("^the new tweet has a preview with title \"(.*)\" and domain \"(.*)\"$")
    public void verifyTweetUrlPreview(String title, String domain) {
        NGTweet latestTweet = getLatestTweet();
        Assert.assertEquals(title, latestTweet.getUrlPreviewTitle());
        Assert.assertEquals(domain, latestTweet.getUrlPreviewAddress());
    }

    @When("^User clicks Like$")
    public void clickLikeForLatestTweet() {
        getLatestTweet().clickLike();
    }

    @Then("^the tweet is liked$")
    public void verifyTweetIsLiked() {
        Assert.assertTrue(getLatestTweet().isLiked());
    }

    @When("^User clicks Unlike$")
    public void clickUnlikeForLatestTweet() {
        getLatestTweet().clickUnlike();
    }

    @Then("^the tweet is not liked$")
    public void verifyTweetIsNotLiked() {
        Assert.assertFalse(getLatestTweet().isLiked());
    }

    @When("^User clicks statistics button$")
    public void clickStatisticsButtonForLatestTweet() {
        getLatestTweet().clickAnalytics();
    }

    @Then("^statistics for tweet are displayed$")
    public void verifyStatisticsDisplayed() {
        homePage.getModalWindow().waitUntilIsVisible(5);
    }

    @When("^User clicks add comment button$")
    public void userClicksAddCommentButton() {
        getLatestTweet().clickReply();
    }

    @Then("^posts from \"(.*)\" are displayed$")
    public void postsFromGivenProfileAreDisplayed(String profile) {
        List<NGTweet> tweets = getFeedPage().getTweets();
        for (NGTweet t: tweets) {
            if (t.getProfileName().equals(profile)) {
                return;
            }
        }
        Assert.fail("Tweets from profile " + profile + " not found");
    }
}
