package steps;

import base.BaseTest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pages.ProfilePage;

public class FollowProfileSteps extends BaseTest {

    @And("^this profile is not followed$")
    public void thisProfileIsNotFollowed() {
        ProfilePage page = getProfilePage();
        if (page.isFollowing()) {
            page.clickUnfollow();
            page.clickConfirmationConfirm();
        }
    }

    @And("^this profile is followed$")
    public void thisProfileIsFollowed() {
        ProfilePage page = getProfilePage();
        if (!page.isFollowing()) {
            page.clickFollow();
        }
    }

    @When("^I click follow$")
    public void clickFollow() {
        getProfilePage().clickFollow();
    }

    @When("^I click unfollow$")
    public void clickUnfollow() {
        getProfilePage().clickUnfollow();
        getProfilePage().clickConfirmationConfirm();
    }

    @Then("^\"(.*)\" is on the list of following profiles$")
    public void isOnTheListOfFollowingProfiles(String profile) {
        Assert.assertTrue(getFollowingPage().getFollowedProfiles().contains(profile));
    }

    @Then("^\"(.*)\" is not on the list of following profiles$")
    public void isNotOnTheListOfFollowingProfiles(String profile) {
        Assert.assertFalse(getFollowingPage().getFollowedProfiles().contains(profile));
    }
}
