package ngelements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NGTweet extends NGHtmlElement {

    private By content = By.cssSelector("div:nth-child(2) > div:nth-child(2) > span");
    private By profileName = By.cssSelector("div:nth-child(2) > div:nth-child(1) a > div > div:nth-child(2) > div > span");
    private By linkPreview = By.cssSelector("div:nth-child(2) > div:nth-child(3)");
    // relative to linkPreview
    private By linkPreviewTitle = By.cssSelector("a[role='link'] > div > div:nth-child(1) > div > span > span");
    private By linkPreviewAddress = By.cssSelector("a[role='link'] > div > div:nth-child(2) > div > span:nth-child(2) > span");

    private By replyButton = By.cssSelector("div[data-testid=\"reply\"]");
    private By retweetButton = By.cssSelector("div[data-testid=\"retweet\"]");
    private By retweetConfirm = By.cssSelector("div[data-testid='retweetConfirm']");
    private By likeButton = By.cssSelector("div[data-testid=\"like\"]");
    private By unlikeButton = By.cssSelector("div[data-testid=\"unlike\"]");
    private By shareButton = By.cssSelector("div:nth-child(2) > div:nth-child(3) > div:nth-child(4) > div[role='button']");
    private By analyticsButton = By.cssSelector("div:nth-child(2) > div:nth-child(3) > div:nth-child(5) > *[role='link']");
    private By moreButton = By.cssSelector("div[data-testid='caret']");
    private By contextMenu = By.cssSelector("div[role='menu']");

    private By deleteMenu = By.cssSelector("div[role='menuitem']:nth-child(1)");

    private By confirmationSheetCancel = By.cssSelector("div[data-testid='confirmationSheetCancel']");
    private By confirmationSheetConfirm = By.cssSelector("div[data-testid='confirmationSheetConfirm']");

    private WebDriver driver;

    public NGTweet(WebElement element, WebDriver driver) {
        this.driver = driver;
        setWrappedElement(element);
    }

    public String getContent() {
        return getWrappedElement().findElement(content).getText();
    }

    public String getProfileName() {
        return getWrappedElement().findElement(profileName).getText().substring(1);
    }

    public String getUrlPreviewTitle() {
        return getWrappedElement().findElement(linkPreview).findElement(linkPreviewTitle).getText();
    }

    public String getUrlPreviewAddress() {
        return getWrappedElement().findElement(linkPreview).findElement(linkPreviewAddress).getText();
    }

    public void deleteTweet() {
        getWrappedElement().findElement(moreButton).click();
        WebElement menu = driver.findElement(contextMenu);
        menu.findElement(deleteMenu).click();
        clickConfirmationConfirm();
    }

    private void clickConfirmationConfirm() {
        driver.findElement(confirmationSheetConfirm).click();
    }

    private void clickConfirmationCancel() {
        driver.findElement(confirmationSheetCancel).click();
    }

    public void clickReply() {
        getWrappedElement().findElement(replyButton).click();
    }

    public void retweet() {
        getWrappedElement().findElement(retweetButton).click();
        driver.findElement(retweetConfirm).click();
    }

    public void clickLike() {
        getWrappedElement().findElement(likeButton).click();
    }

    public void clickUnlike() {
        getWrappedElement().findElement(unlikeButton).click();
    }

    public boolean isLiked() {
        return getWrappedElement().findElements(unlikeButton).size() > 0;
    }

    public void clickShare() {
        getWrappedElement().findElement(shareButton).click();
    }

    public void clickAnalytics() {
        getWrappedElement().findElement(analyticsButton).click();
    }
}
