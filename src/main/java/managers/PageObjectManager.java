package managers;

import base.BasePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pages.FollowingPage;
import pages.HomePage;
import pages.LoginPage;

import java.lang.reflect.Method;

public class PageObjectManager {

    protected static final Logger log = LogManager.getLogger(Logger.class.getName());

    private WebDriver driver;
    private HomePage homePage;
    private LoginPage loginPage;
    private FollowingPage followingPage;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return (homePage == null) ? homePage = new HomePage(this.driver) : homePage;
    }

    public FollowingPage getFollowingPage() {
        return (followingPage == null) ? followingPage = new FollowingPage(this.driver) : followingPage;
    }

    public BasePage getPageByName(String pageName) {
        pageName = pageName.replace(" ", "");
        String methodName = "get" + pageName + "Page";

        BasePage page = null;

        try {
            Method method = PageObjectManager.class.getMethod(methodName);
            page = (BasePage) method.invoke(this);
        } catch (Exception e) {
            log.error(String.format("There is no such method: %s", methodName));
            Assert.fail(e.getMessage());
        }

        return page;
    }

    public LoginPage getLoginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage(driver);
        }

        return loginPage;
    }
}
