package base;

import managers.Context;
import managers.JSExecutor;
import managers.PageObjectManager;
import managers.ZAPManager;
import ngelements.NGTweet;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pages.FeedPage;
import pages.FollowingPage;
import pages.ProfilePage;

public abstract class BaseTest {

    protected static final Logger log = LogManager.getLogger(Logger.class.getName());
    protected static Context context;
    protected static PageObjectManager pages;
    protected static ZAPManager zapManager;
    protected static JSExecutor jsExecutor;

    protected NGTweet getLatestTweet() {
        FeedPage page = (FeedPage) context.currentPage;
        return page.getTweets().get(0);
    }

    protected ProfilePage getProfilePage() {
        return (ProfilePage) context.currentPage;
    }

    protected FeedPage getFeedPage() {
        return (FeedPage) context.currentPage;
    }

    protected FollowingPage getFollowingPage() {
        return (FollowingPage) context.currentPage;
    }
}
