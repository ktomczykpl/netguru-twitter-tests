package pages;

import base.BasePage;
import ngelements.NGButton;
import ngelements.NGHtmlElement;
import ngelements.NGTextInput;
import ngelements.NGTweet;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public abstract class FeedPage extends BasePage {
    private By alertLocator = By.cssSelector("div[role='alert']");
    @FindBy(css = "div[role='alert']")
    private NGHtmlElement alert;

    @FindBy(css = "div[aria-labelledby='modal-header']")
    private NGHtmlElement modal;

    private By alertText = By.cssSelector("div > span");

    @FindBy(css = "div[data-testid='tweetTextarea_0']")
    private NGTextInput tweetCommentInput;

    @FindBy(css = "div[data-testid='tweetButton']")
    private NGButton tweetButton;

    public FeedPage(WebDriver driver, String relativeUrl) {
        super(driver, relativeUrl);
    }

    public FeedPage(WebDriver driver) {
        super(driver);
    }

    public List<NGTweet> getTweets(){
        return driver.findElements(By.cssSelector("div[data-testid='tweet']")).stream()
                .map(element -> new NGTweet(element, driver))
                .collect(Collectors.toList());
    }

    public String getAlertText() {
        alert.waitUntilIsVisible(20);
        String text = alert.findElement(alertText).getText();
        log.info("Twitter alert: " + text);
        return text;
    }

    public NGHtmlElement getModalWindow() {
        return modal;
    }

    // TODO: should be replaced with explicit wait
    private void waitX() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
        }
    }

    public void enterTweetComment(String comment) {
        tweetCommentInput.sendKeys(comment);
        tweetButton.click();
        getAlertText();
        waitX();
    }
}
