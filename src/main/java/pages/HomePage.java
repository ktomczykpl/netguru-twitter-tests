package pages;

import ngelements.NGButton;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends FeedPage {

    @FindBy(css = ".DraftEditor-editorContainer > div")
    private WebElement tweetTextInput;

    @FindBy(css = "div[data-testid='tweetButtonInline']")
    private NGButton tweetSendButton;

    public HomePage(WebDriver driver) {
        super(driver, "home");
    }

    @Override
    protected void load() {
        driver.get(getUrl());
    }

    @Override
    protected void isLoaded() throws Error {
        Assert.assertEquals(getUrl(), driver.getCurrentUrl());
    }

    public void enterTweetText(String text) {
        tweetTextInput.sendKeys(text);
    }

    public void clickSend() {
        tweetSendButton.click();
        getAlertText();
    }

    public NGButton getSendButton() {
        return tweetSendButton;
    }
}
