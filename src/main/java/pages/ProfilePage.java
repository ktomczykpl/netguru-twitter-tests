package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class ProfilePage extends FeedPage {

    @FindBy(css = "div[data-testid='placementTracking'] div[role='button']")
    private By followButton = By.cssSelector("div[data-testid='placementTracking'] div[data-testid$='-follow']");
    private By unfollowButton = By.cssSelector("div[data-testid='placementTracking'] div[data-testid$='-unfollow']");

    private By confirmationSheetCancel = By.cssSelector("div[data-testid='confirmationSheetCancel']");
    private By confirmationSheetConfirm = By.cssSelector("div[data-testid='confirmationSheetConfirm']");

    public ProfilePage(WebDriver driver, String profileName) {
        super(driver, profileName);
    }

    @Override
    protected void load() {
        driver.get(getUrl());
    }

    @Override
    protected void isLoaded() throws Error {
        Assert.assertEquals(getUrl(), driver.getCurrentUrl());
    }

    public void clickFollow() {
        driver.findElement(followButton).click();
    }

    public void clickUnfollow() {
        driver.findElement(unfollowButton).click();
    }

    public boolean isFollowing() {
        return driver.findElements(unfollowButton).size() > 0;
    }

    public void clickConfirmationConfirm() {
        driver.findElement(confirmationSheetConfirm).click();
    }
}
