package pages;

import base.BasePage;
import ngelements.NGButton;
import ngelements.NGTextInput;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    @FindBy(css = "input[name=\"session[username_or_email]\"]")
    private NGTextInput loginInput;

    @FindBy(css = "input[name=\"session[password]\"]")
    private NGTextInput passwordInput;

    @FindBy(css = "[data-testid=\"LoginForm_Login_Button\"]")
    private NGButton loginButton;

    public LoginPage(WebDriver driver) {
        super(driver, "login");
    }

    public void login(String userName, String password){
        loginInput.sendKeys(userName);
        passwordInput.sendVulnerableData(password);
        loginButton.click();
    }
    @Override
    protected void load() {
        driver.get(getUrl());
    }

    @Override
    protected void isLoaded() throws Error {
        Assert.assertEquals(getUrl(), driver.getCurrentUrl());
    }
}
