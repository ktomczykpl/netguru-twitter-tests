package pages;

import base.BasePage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.List;
import java.util.stream.Collectors;

public class FollowingPage extends BasePage {
    private By followedProfile = By.cssSelector("div[data-testid='UserCell'] > div > div:nth-child(2) > div:nth-child(1) a > div > div:nth-child(2) span");

    public FollowingPage(WebDriver driver) {
        super(driver, "following");
    }

    @Override
    protected void load() {
        driver.get(getUrl());
    }

    @Override
    public String getUrl() {
        return baseUrl + "/" + propertiesLoader.getUsername() + relativeUrl;
    }

    @Override
    protected void isLoaded() throws Error {
        Assert.assertEquals(getUrl(), driver.getCurrentUrl());
    }

    public List<String> getFollowedProfiles() {
        return driver.findElements(followedProfile).stream().map(element -> element.getText().substring(1)).collect(Collectors.toList());
    }
}
